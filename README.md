# Aide et ressources de XRSTools pour Synchrotron SOLEIL

## Résumé
- Outils de réduction de données Raman X 
- Open source

## Sources
- Code source: https://gitlab.esrf.fr/mirone/XRStools
- Documentation officielle: http://ftp.esrf.fr/scisoft/XRStools/index.html

## Installation
- Systèmes d'exploitation supportés: Windows, Linux, MacOS, Machine virtuelle
- Installation: Difficile (très technique)

## Format de données
- en entrée: hdf5, edf
- en sortie: h5
- sur la Ruche
